FROM python:alpine3.10
RUN apk add --update bash && rm -rf /var/cache/apk/*
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt