from decimal import Decimal
from unittest.mock import patch
import datetime

from telecomunications.models import (
    Call,
    Client,
)
from telecomunications.rules import (
    rule_weekend,
    rule_late_night,
    rule_regular,
)


def test_rule_weekend():
    datetime_init = '2020-01-18T10:02:30.000Z'
    datetime_end = '2020-01-18T10:02:40.000Z'
    call_type = Call.LOCAL
    call = Call(datetime_init, datetime_end, call_type)
    client = None
    price = rule_weekend(client, call)
    price_expected = Decimal((0, (0, 1), -2))

    assert price == price_expected


def test_not_rule_weekend():
    datetime_init = '2020-01-21T10:02:30.000Z'
    datetime_end = '2020-01-21T10:02:40.000Z'
    call_type = Call.LOCAL
    call = Call(datetime_init, datetime_end, call_type)
    client = None
    price = rule_weekend(client, call)
    price_expected = None

    assert price == price_expected


def test_rule_late_night_afternoon():
    datetime_init = '2020-01-21T23:02:30.000Z'
    datetime_end = '2020-01-21T23:02:40.000Z'
    call_type = Call.LOCAL
    call = Call(datetime_init, datetime_end, call_type)
    client = None
    price = rule_late_night(client, call)
    price_expected = Decimal((0, (0, 2), -2))

    assert price == price_expected


def test_rule_late_night_morning():
    datetime_init = '2020-01-21T01:02:30.000Z'
    datetime_end = '2020-01-21T01:02:40.000Z'
    call_type = Call.LOCAL
    call = Call(datetime_init, datetime_end, call_type)
    client = None
    price = rule_late_night(client, call)
    price_expected = Decimal((0, (0, 2), -2))

    assert price == price_expected


def test_not_rule_late_night_afternoon():
    datetime_init = '2020-01-21T14:02:30.000Z'
    datetime_end = '2020-01-21T14:02:40.000Z'
    call_type = Call.LOCAL
    call = Call(datetime_init, datetime_end, call_type)
    client = None
    price = rule_late_night(client, call)
    price_expected = None

    assert price == price_expected


def test_not_rule_late_night_morning():
    datetime_init = '2020-01-21T10:02:30.000Z'
    datetime_end = '2020-01-21T10:02:40.000Z'
    call_type = Call.LOCAL
    call = Call(datetime_init, datetime_end, call_type)
    client = None
    price = rule_late_night(client, call)
    price_expected = None

    assert price == price_expected


def test_rule_regular_new_client():
    datetime_init = '2020-01-21T10:02:30.000Z'
    datetime_end = '2020-01-21T10:03:30.000Z'
    call_type = Call.LOCAL
    call = Call(datetime_init, datetime_end, call_type)
    date_joined = '2020-01-01'
    client = Client(date_joined)
    with patch('telecomunications.models.datetime') as mock_datetime:
        datetime_today = datetime.datetime(year=2020, month=1, day=30, hour=1, minute=0, second=0)
        mock_datetime.today.return_value = datetime_today

        price = rule_regular(client, call)
    price_expected = Decimal((0, (0, 2), -2))

    assert price == price_expected


def test_rule_regular_not_new_client():
    datetime_init = '2020-01-21T10:02:30.000Z'
    datetime_end = '2020-01-21T10:02:30.000Z'
    call_type = Call.LOCAL
    call = Call(datetime_init, datetime_end, call_type)
    date_joined = '2020-01-01'
    client = Client(date_joined)
    with patch('telecomunications.models.datetime') as mock_datetime:
        datetime_today = datetime.datetime(year=2020, month=2, day=15, hour=1, minute=0, second=0)
        mock_datetime.today.return_value = datetime_today

        price = rule_regular(client, call)
    price_expected = Decimal((0, (0, 5), -2))

    assert price == price_expected