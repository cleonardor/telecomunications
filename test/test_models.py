import pytest
import pytz
from datetime import datetime, date
from unittest.mock import patch
from decimal import Decimal

from telecomunications.models import (
    Call,
    Client,
    Plan,
)
from telecomunications.rules import (
    rule_weekend,
    rule_late_night,
    rule_regular,
)


#--------------------------------------------------------------------------------------------------
# Call tests
#--------------------------------------------------------------------------------------------------

def test_call_with_bad_datetime():
    datetime_init = '2020-01-20T10:02:30.000Z'
    datetime_end = '2020-01-20T30:02:30.000Z'
    with pytest.raises(ValueError):
        call = Call(datetime_init, datetime_end, Call.LOCAL)


def test_call_with_bad_call_type():
    datetime_init = '2020-01-20T10:02:30.000Z'
    datetime_end = '2020-01-20T10:02:35.000Z'
    call_type = 'Unknown'
    with pytest.raises(ValueError):
        call = Call(datetime_init, datetime_end, call_type)


def test_call_with_right_data():
    datetime_init = '2020-01-20T10:02:30.000Z'
    datetime_end = '2020-01-20T10:02:35.000Z'
    call_type = Call.LOCAL
    call = Call(datetime_init, datetime_end, call_type)
    datetime_init_expected = datetime(year=2020, month=1, day=20, hour=10, minute=2, second=30, tzinfo=pytz.utc)
    datetime_end_expected = datetime(year=2020, month=1, day=20, hour=10, minute=2, second=35, tzinfo=pytz.utc)

    assert call.datetime_init == datetime_init_expected
    assert call.datetime_end == datetime_end_expected
    assert call.call_type == call_type


def test_call_is_local():
    datetime_init = '2020-01-20T10:02:30.000Z'
    datetime_end = '2020-01-20T10:02:35.000Z'
    call_type = Call.LOCAL
    call = Call(datetime_init, datetime_end, call_type)

    assert call.is_local()


def test_call_is_not_local():
    datetime_init = '2020-01-20T10:02:30.000Z'
    datetime_end = '2020-01-20T10:02:35.000Z'
    call_type = Call.INTERNATIONAL
    call = Call(datetime_init, datetime_end, call_type)

    assert not call.is_local()


def test_call_is_international():
    datetime_init = '2020-01-20T10:02:30.000Z'
    datetime_end = '2020-01-20T10:02:35.000Z'
    call_type = Call.INTERNATIONAL
    call = Call(datetime_init, datetime_end, call_type)

    assert call.is_international()


def test_call_is_not_international():
    datetime_init = '2020-01-20T10:02:30.000Z'
    datetime_end = '2020-01-20T10:02:35.000Z'
    call_type = Call.LOCAL
    call = Call(datetime_init, datetime_end, call_type)

    assert not call.is_international()


def test_call_duration():
    datetime_init = '2020-01-20T10:02:30.000Z'
    datetime_end = '2020-01-20T10:02:45.000Z'
    call_type = Call.INTERNATIONAL
    call = Call(datetime_init, datetime_end, call_type)
    duration_expected = Decimal((0, (2, 5), -2))

    assert call.duration() == duration_expected

#--------------------------------------------------------------------------------------------------
# Client tests
#--------------------------------------------------------------------------------------------------

def test_client_with_bad_date():
    date_join = '202001-01'
    with pytest.raises(ValueError):
        client = Client(date_join)


def test_client_right_data():
    date_joined = '2020-01-01'
    client = Client(date_joined)
    date_joined_expected = datetime(year=2020, month=1, day=1, hour=0, minute=0)

    assert client.date_joined == date_joined_expected


def test_client_is_new_days_default(monkeypatch):
    with patch('telecomunications.models.datetime') as mock_datetime:
        datetime_today = datetime(year=2020, month=1, day=18, hour=1, minute=0, second=0)
        mock_datetime.today.return_value = datetime_today
        date_joined = '2020-01-01'
        client = Client(date_joined)

        assert client.is_new()


def test_client_is_new_days_given(monkeypatch):
    with patch('telecomunications.models.datetime') as mock_datetime:
        datetime_today = datetime(year=2020, month=2, day=18, hour=1, minute=0, second=0)
        mock_datetime.today.return_value = datetime_today
        date_joined = '2020-01-01'
        client = Client(date_joined)

        assert client.is_new(days=60)


def test_client_is_not_new():
    with patch('telecomunications.models.datetime') as mock_datetime:
        datetime_today = datetime(year=2020, month=2, day=18, hour=1, minute=0, second=0)
        mock_datetime.today.return_value = datetime_today
        date_joined = '2020-01-01'
        client = Client(date_joined)

        assert not client.is_new()


def test_client_is_not_new_days_given():
    with patch('telecomunications.models.datetime') as mock_datetime:
        datetime_today = datetime(year=2020, month=3, day=18, hour=1, minute=0, second=0)
        mock_datetime.today.return_value = datetime_today
        date_joined = '2020-01-01'
        client = Client(date_joined)

        assert not client.is_new(days=60)


def test_client_get_cost_bill_with_plan():
    datetime_init = '2020-01-20T10:02:30.000Z'
    datetime_end = '2020-01-20T10:02:45.000Z'
    call_type = Call.LOCAL
    call1 = Call(datetime_init, datetime_end, call_type)
    datetime_init = '2020-01-20T11:02:30.000Z'
    datetime_end = '2020-01-20T11:03:00.000Z'
    call_type = Call.INTERNATIONAL
    call2 = Call(datetime_init, datetime_end, call_type)
    date_joined = '2020-01-01'
    client = Client(date_joined)
    client.load_calls([call1, call2])
    rules = [rule_weekend, rule_late_night, rule_regular]
    plan = Plan(rules)
    client.set_plan(plan)
    with patch('telecomunications.models.datetime') as mock_datetime:
        datetime_today = datetime(year=2020, month=1, day=18, hour=1, minute=0, second=0)
        mock_datetime.today.return_value = datetime_today
        cost = client.get_bill_cost()

    # local call with regular cost made by new client: 0.02 x minute (call is 15 seg duration)
    cost_expected = Decimal((0, (0, 2), -2)) * Decimal((0, (2, 5), -2))
    # internation call with regular cost made by new client: 0.04 x minute (call is 30 seg duration)
    cost_expected += Decimal((0, (0, 4), -2)) * Decimal((0, (0, 5), -1))
    assert cost == cost_expected


def test_client_get_cost_bill_without_plan():
    datetime_init = '2020-01-20T10:02:30.000Z'
    datetime_end = '2020-01-20T10:02:45.000Z'
    call_type = Call.LOCAL
    call1 = Call(datetime_init, datetime_end, call_type)
    datetime_init = '2020-01-20T11:02:30.000Z'
    datetime_end = '2020-01-20T11:03:00.000Z'
    call_type = Call.INTERNATIONAL
    call2 = Call(datetime_init, datetime_end, call_type)
    date_joined = '2020-01-01'
    client = Client(date_joined)
    client.load_calls([call1, call2])
    with patch('telecomunications.models.datetime') as mock_datetime:
        datetime_today = datetime(year=2020, month=1, day=18, hour=1, minute=0, second=0)
        mock_datetime.today.return_value = datetime_today
        cost = client.get_bill_cost()

    cost_expected = None
    assert cost == cost_expected

#--------------------------------------------------------------------------------------------------
# Plan tests
#--------------------------------------------------------------------------------------------------

def test_plan_get_call_cost_local():
    date_joined = '2020-01-01'
    client = Client(date_joined)
    datetime_init = '2020-01-20T10:02:30.000Z'
    datetime_end = '2020-01-20T10:02:45.000Z'
    call_type = Call.LOCAL
    call = Call(datetime_init, datetime_end, call_type)
    rules = [rule_weekend, rule_late_night, rule_regular]
    plan = Plan(rules)
    with patch('telecomunications.models.datetime') as mock_datetime:
        datetime_today = datetime(year=2020, month=1, day=18, hour=1, minute=0, second=0)
        mock_datetime.today.return_value = datetime_today
        cost = plan._get_call_cost(client, call)

    # local call with regular cost made by new client: 0.02 x minute (call is 15 seg duration)
    cost_expected = Decimal((0, (0, 2), -2)) * Decimal((0, (2, 5), -2))
    assert cost == cost_expected


def test_plan_get_call_cost_international():
    date_joined = '2020-01-01'
    client = Client(date_joined)
    datetime_init = '2020-01-20T10:02:30.000Z'
    datetime_end = '2020-01-20T10:02:45.000Z'
    call_type = Call.INTERNATIONAL
    call = Call(datetime_init, datetime_end, call_type)
    rules = [rule_weekend, rule_late_night, rule_regular]
    plan = Plan(rules)
    with patch('telecomunications.models.datetime') as mock_datetime:
        datetime_today = datetime(year=2020, month=1, day=18, hour=1, minute=0, second=0)
        mock_datetime.today.return_value = datetime_today
        cost = plan._get_call_cost(client, call)

    # local call with regular cost made by new client: 0.02 x minute (call is 15 seg duration)
    cost_expected = Decimal((0, (0, 2), -2)) * Decimal((0, (2, 5), -2)) * 2
    assert cost == cost_expected


def test_plan_get_cost_bill():
    datetime_init = '2020-01-20T10:02:30.000Z'
    datetime_end = '2020-01-20T10:02:45.000Z'
    call_type = Call.LOCAL
    call1 = Call(datetime_init, datetime_end, call_type)
    datetime_init = '2020-01-20T11:02:30.000Z'
    datetime_end = '2020-01-20T11:03:00.000Z'
    call_type = Call.INTERNATIONAL
    call2 = Call(datetime_init, datetime_end, call_type)
    date_joined = '2020-01-01'
    client = Client(date_joined)
    client.load_calls([call1, call2])
    rules = [rule_weekend, rule_late_night, rule_regular]
    plan = Plan(rules)
    with patch('telecomunications.models.datetime') as mock_datetime:
        datetime_today = datetime(year=2020, month=1, day=18, hour=1, minute=0, second=0)
        mock_datetime.today.return_value = datetime_today
        cost = plan.get_bill_cost(client)

    # local call with regular cost made by new client: 0.02 x minute (call is 15 seg duration)
    cost_expected = Decimal((0, (0, 2), -2)) * Decimal((0, (2, 5), -2))
    # internation call with regular cost made by new client: 0.04 x minute (call is 30 seg duration)
    cost_expected += Decimal((0, (0, 4), -2)) * Decimal((0, (0, 5), -1))
    assert cost == cost_expected


def test_plan_get_cost_bill_without_calls():
    date_joined = '2020-01-01'
    client = Client(date_joined)
    rules = [rule_weekend, rule_late_night, rule_regular]
    plan = Plan(rules)
    with patch('telecomunications.models.datetime') as mock_datetime:
        datetime_today = datetime(year=2020, month=1, day=18, hour=1, minute=0, second=0)
        mock_datetime.today.return_value = datetime_today
        cost = plan.get_bill_cost(client)

    cost_expected = Decimal()
    assert cost == cost_expected