# Description

## Some rules to follow:

* Unless told otherwise please submit your response before 8:00am on the monday following your first interview so your submission can be reviewed in time.  If you are unable to do this because of other commitments please notify us beforehand.
* Do NOT upload your code to public websides such as github
* Only submit source code & any required documentation please (fileouts if using Smalltalk), no binaries or smalltalk images please as the email will be filtered and we will not be able to review your submission.

## Required

A telecom company wants to implement a simple system for call billing.  

In their call plans there are different types of tariff:
* regular calls at 0.05 pence per minute
* late night calls (for example from 10pm to 4am) at 0.02 pence per minute
* weekend calls at 0.01 pence per minute

There are two kinds of clients that the company manages:
* New clients who have been given a discount on their regular calls (regular calls pay the same rate as late night calls)
* Existing clients who pay the standard rates mentioned above

In addition the company uses the following rule to charge for local & international calls:
* International calls double the rate per minute

The company is interested in implementing a simple billing system to calculate the total charge to a client given their call history.

Please use an object oriented language of your choice to allow the company to calculate the total charge to a client given their call history.

### Notes:
* You DO NOT need to implement any UI for this exercise.
* You can assume that the call history data is already given to you (you do not need to write code to load it from a file, database or similar source).
* All the rules the company requires to calculate the charge for a call are given above.  There are no other rules to apply.
* You do not need to worry about determining if a call is local or international.  You can assume there is a method that can do that for you.


# Code

## Code Observations

- Datetime input format is assumed in ISO format but it could be changed later.
- I assume an user is new if they joined to the company less than one month before.
- A plan should have a list of rules. Those rules have a precedence, if a rule is matched rules remainder will be discarted.
- Every rule is a function that should receive a client and a call and return a Decimal (price x minute) if the rule checks otherwise return None. 


## Deployment

The following details show how to deploy and run this application.

### Docker run

Go to the project root folder.
```bash
    $ docker build -t telecom_image .
    $ docker run -it --name telecom -v $PWD:/app telecom_image /bin/bash
```

### Program and tests run

Once inside the container you can run the **telecom** program and **tests**

* run all tests with module output results
```bash
    $ pytest
```
* run all tests with test output results
```bash
    $ pytest -v
```
