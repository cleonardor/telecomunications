from decimal import Decimal
from dateutil import parser
from datetime import datetime


class Call:
    '''
    Represent a call
    Input:
        datetime_init:String
            datetime is ISO format when the call begin
        datetime_end:String
            datetime is ISO format when the call end
        call_type:String
            type of the call, it could be 'local' or 'international'
    '''
    LOCAL = 'LC'
    INTERNATIONAL = 'IT'

    def __init__(self, datetime_init, datetime_end, call_type):
        self._validate_data(call_type)
        self.datetime_init = parser.parse(datetime_init)
        self.datetime_end = parser.parse(datetime_end)
        self.call_type = call_type

    def is_local(self):
        '''
        Find if a call is local or no
        Input:
            None
        Output:
            bool
        '''
        return self.call_type == self.LOCAL

    def is_international(self):
        '''
        Find if a call is international or no
        Input:
            None
        Output:
            bool
        '''
        return self.call_type == self.INTERNATIONAL

    def duration(self):
        '''
        Find the call duration in minutes
        Input:
            None
        Output
            int
        '''
        diff = self.datetime_end - self.datetime_init
        return Decimal(diff.seconds / 60)

    def _validate_data(self, call_type):
        '''
        Verify in the call_type given is right. If not raise an exception
        Input:
            call_type:String
        Output:
            None
        '''
        if call_type not in (self.LOCAL, self.INTERNATIONAL):
            raise ValueError('invalid call type')


class Client:
    '''
    Represent a client
    Input:
        date_joined:String
            date in ISO format when client joined to the company
    '''
    def __init__(self, date_joined):
        self.date_joined = parser.parse(date_joined)
        self.calls = []
        self.plan = None

    def load_calls(self, calls):
        '''
        Load to the client a list of Call objects
        Input:
            calls:List<Call>
        Output:
            None
        '''
        self.calls += calls

    def is_new(self, days=30):
        '''
        Find if the client is new or not depending of amount of days
        Input:
            days: amount of days after client join to the company and they are 
            considerer a new client
        Output: bool
        '''
        is_new = True
        today = datetime.today()
        diff = today - self.date_joined
        if diff.days > days:
            is_new =  False
        return is_new

    def set_plan(self, plan):
        '''
        Set the plan to the client
        Input:
            plan:Plan
        Output:
            None
        '''
        self.plan = plan

    def get_bill_cost(self):
        '''
        Get bill cost depending of plan and history calls
        Input:
            None
        Output
            Decimal if plan exist otherwise None
        '''
        cost = None
        if self.plan:
            cost = self.plan.get_bill_cost(self)
        return cost

    def get_calls(self):
        '''
        Get the calls client
        '''
        return self.calls


class Plan:
    '''
    Represent a Telecom Plan
    Input:
        rules:List<rule>.
            'rule' should be a funtion which recive a 'client' and a 'call' and return a 'price'
            (Decimal) if the rule is checked otherwise return None.
            e.x. rules = [rule_weekend, rule_regular] (see rules module for definitios)
    '''
    def __init__(self, rules):
        self.rules = rules

    def get_bill_cost(self, client):
        '''
        Get bill cost depending of plan rules and calls client
        Input:
            client:Client
        Output:
            Decimal
        '''
        calls = client.get_calls()
        total_cost = Decimal()
        for call in calls:
            total_cost += self._get_call_cost(client, call)
        return total_cost

    def _get_call_cost(self, client, call):
        '''
        Get call cost depending of plan rules and client
        Input:
            client:Client
        Output:
            Decimal
        '''
        for rule in self.rules:
            price = rule(client, call)
            if price:
                break
        if call.is_international():
            price *= 2
        cost = price * call.duration()
        return cost