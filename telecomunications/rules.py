from decimal import Decimal
import datetime


def rule_weekend(client, call):
    '''
    Check if call date init is a weekend day (Saturday or Sunday)
    '''
    price = None
    if call.datetime_init.isoweekday() in [6, 7]:
        price = Decimal((0, (0, 1), -2))
    return price


def rule_late_night(client, call):
    '''
    Check if call time init is between 10 pm and 4 am
    '''
    price = None
    call_time = call.datetime_init.time()
    limit_night_under = datetime.time(hour=22)
    limit_night_over = datetime.time(hour=23, minute=59)
    limit_morning_under = datetime.time(hour=0)
    limit_morning_over = datetime.time(hour=4)
    if (call_time >= limit_night_under and call_time <= limit_night_over) or \
       (call_time >= limit_morning_under and call_time <= limit_morning_over):
        price = Decimal((0, (0, 2), -2))
    return price


def rule_regular(client, call):
    '''
    Check is the client is new or not and asign a price depending of it
    '''
    cent = 5
    if client.is_new():
        cent = 2
    price = Decimal((0, (0, cent), -2))
    return price